<?php

namespace App\System;

class ProgramList
{
  private array $list = [];

  private int $count = 0;

  private array $installed = [];

  public function __construct(array $list)
  {
    $this->setList($list);
    $this->setCount($list);
  }

  /**
   * @param array $list
   */
  private function setList(array $list): void
  {
    $this->list = $list;
  }

  /**
   * @param array $list
   * @return void
   */
  private function setCount(array $list): void
  {
    $this->count = count($list);
  }

  /**
   * @return array
   */
  public function getList(): array
  {
    return $this->list;
  }

  /**
   * @return int
   */
  public function getCount(): int
  {
    return $this->count;
  }

  /**
   * @return array
   */
  public function getInstalled(): array
  {
    return $this->installed;
  }

  /**
   * @param array $installed
   */
  private function setInstalled(\App\System\Program\Program $program): void
  {
    $this->installed[$program->getProgramName()] = $program;
  }

  public function initialization()
  {
    foreach ($this->getList() as $number => $required_program) {
      $program = ProgramFactory::create($required_program, [
        'text' => \App\Modules\Text::class,
        'console' => \App\System\Console::class
      ]);

      if (!$program->hasProgram()) {
        try {
          if (!$program->install()) {
            throw new \Exception('Program ' . $program->getProgramName() . ' not installed.');
          }
        } catch (\Exception $e) {
          echo $e->getMessage();
        }
      }

      $this->setInstalled($program);

      \App\Modules\LoopProcessInfo::render([
        'current' => ++$number,
        'all' => $this->getCount(),
        'title' => '',
        'subtitle' => 'Checking program: ' . $program->getProgramName(),
      ]);
    }
  }

  public static function checking()
  {

  }
}