<?php

namespace App\System;

use App\System\Console;
use App\Middleware\System\Execute;
use App\System\Program\Program;
use JetBrains\PhpStorm\ArrayShape;

class Storage
{
  use Execute;

  private static array $config;

  private static array $states;

  private static object $console;

  private static object $projects_settings;

  private static object|null $instance = null;

  public function __constructor(Console $console)
  {
    return (self::$instance || self::getInstance($console));
  }

  public static function getInstance(Console $console): object
  {
    if (is_null(self::$instance)) {
      self::$instance = new self();

      self::$console = $console;
      self::initConfig();
    }

    return self::$instance;
  }

  private static function initConfig(bool $force_update = false): void
  {
    global $projects_settings;

    self::$projects_settings = $projects_settings;

    self::directoryInitialization();

    self::$config = [
      'free_space' => self::getFreeSpacingState(),
      'min_free_space' => self::$projects_settings->storage_min_size,
      'location' => self::$projects_settings->location,
      'has_access' => self::checkDir() ? 1 : 0,
    ];

    self::$config['states'] = self::getState($force_update);
  }

  private static function refresh_states(): void
  {
    $list = [];

    $free_space_value = preg_replace('/[A-Z]/', '', self::$config['free_space']);

    $list['free_space'] = ($free_space_value < self::$config['min_free_space'] ? 'Low' : 'Enough') . ' free space for projects.';
    $list['has_access'] = self::createDir(self::$projects_settings->location);

    self::$states = $list;
  }

  private static function directoryInitialization(): void
  {
    if (!self::checkDir(self::$projects_settings->location)) {
      self::createDir(self::$projects_settings->location);
    }
  }

  private static function getState(bool $force_update = false): array
  {
    if (empty(self::$states) || $force_update) {
      self::refresh_states();
    }

    return self::$states;
  }

  public static function getFreeSpacingState(string $path = null): string
  {
    return self::execute('df -h --output=avail ' . ($path ?? self::$projects_settings->location) . ' | tail -n1');
  }

  public static function checkDir(string $path = null): bool
  {
    return (file_exists($path ?? self::$projects_settings->location));
  }

  /**
   * @param string|null $path
   * @return string
   */
  public static function createDir(string $path = null): string
  {
    if (self::checkDir($path)) {
      return sprintf('Directory "%s" already exists.', $path);
    }

    try {
      if (!is_null($path) && !mkdir($path) && !is_dir($path)) {
       return throw new \RuntimeException(sprintf('Directory "%s" was not created.', $path));
      }

      return sprintf('Directory "%s" created.', $path);
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }

  public function getConfig(): array
  {
    self::initialization();

    return self::$config;
  }

  public static function initialization(bool $force_update = false): void
  {
    if (empty(self::$config) || $force_update) {
      self::initConfig($force_update);
    }
  }
}