<?php

namespace App\System;

class Configs
{
  protected string $namespace = '';
  protected string $path = '';
  protected array|object $temp_data = [];
  protected array $configs = [];
  
  protected function has_namespace(): bool
  {
    if (empty($this->configs[$this->namespace])) {
      return false;
    }
    
    return true;
  }
  
  protected function read(): array|object
  {
    if (!file_exists($this->path)) {
      return [];
    }
    
    $this->temp_data = json_decode(file_get_contents($this->path));
    
    return $this->temp_data;
  }
  
  private function clear_temp_data(): void
  {
    $this->temp_data = [];
  }
  
  protected function add(): bool
  {
    return (($this->configs[$this->namespace] = $this->temp_data) && $this->clear_temp_data());
  }
  
  private static ?Configs $instance = null;
  
  public static function getInstance(): Configs
  {
    if (static::$instance === null) {
      static::$instance = new static();
    }
    
    return static::$instance;
  }
  
  private function __construct()
  {
  }
  
  private function __clone()
  {
  }
  
  private function init_params(string $path, string $namespace): void
  {
    $this->namespace = $namespace;
    $this->path = $path;
  }
  
  public function load(string $path, string $namespace = 'common'): bool
  {
    $this->init_params($path, $namespace);
    
    if (!$this->has_namespace()) {
      if (!empty($this->read())) {
        return $this->add();
      }
      
      return false;
    }
    
    return $this->is_loaded();
  }
  
  public function is_loaded(string $namespace = ''): bool
  {
    $namespace = empty($namespace) ? $this->namespace : $namespace;
    return (!empty($this->configs[$namespace]));
  }
  
  public function get(string $namespace = 'common'): object|array
  {
    return (!empty($namespace) && $this->is_loaded($namespace)) ? $this->configs[$namespace] : $this->configs;
  }
  
  public function set(string $namespace = 'common', string $key = null, $value = null): bool
  {
    if (!is_null($key) && !is_null($value)) {
      
      if (!isset($this->configs[$namespace])) {
        $this->configs[$namespace] = [];
      }
      
      if (is_object($this->configs[$namespace])) {
        $this->configs[$namespace]->$key = $value;
      } else {
        $this->configs[$namespace][$key] = $value;
      }
      
      return true;
    }
    
    return false;
  }
  
  public function init_path($data = null, string $root = '/'): string
  {
    if (is_string($data)) {
      return $root . $data;
    }
    
    if (empty($data)) {
      return false;
    }
    
    foreach ($data as &$item) {
      $item = $root . $item;
    }
    
    return $data;
  }
}