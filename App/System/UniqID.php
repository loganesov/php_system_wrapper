<?php

namespace App\System;

class UniqID
{
  private static string $uniq_id = '';
  private static string $file_path = '';
  private static ?UniqID $instance = null;
  
  public static function getInstance($file_path): UniqID
  {
    if (static::$instance === null) {
      static::$instance = new static();
      self::$file_path = $file_path;
    }
    
    return static::$instance;
  }
  
  private function __construct()
  {
  }
  
  private function __clone()
  {
  }
  
  private static function read_file(): string
  {
    self::$uniq_id = file_get_contents(self::$file_path);
    return self::$uniq_id;
  }
  
  public function get()
  {
    if (!empty(self::$uniq_id)) {
      return self::$uniq_id;
    }
    
    if (file_exists(self::$file_path)) {
      return self::read_file();
    }
    
    return self::create();
  }
  
  private static function create()
  {
    $info = explode(' ', php_uname());
    
    file_put_contents(self::$file_path, self::clear($info[0]) . '_' . self::clear($info[1]) . '_' . self::clear(uniqid('_', false)));
    
    return self::read_file();
  }
  
  private static function clear($str)
  {
    foreach (['-', ' '] as $search) {
      $str = str_replace($search, '_', $str);
    }
    
    return strtolower($str);
  }
}