<?php

namespace App\System;

use \App\System\Program;
use JetBrains\PhpStorm\Pure;

class ProgramFactory
{
  /**
   * @var array
   */
  private static array $modules = [];

  private function __construct()
  {
  }

  /**
   * @param $raw_text
   * @return string
   */
  #[Pure]
  public static function prepareClassName(string $raw_text): string
  {
    if (empty($raw_text)) {
      return '';
    }

    $className = $raw_text;

    if (!empty(self::$modules) && isset(self::$modules['text'])) {
      $className = self::$modules['text']::convertDashToUpperCase($raw_text, '-');
    }

    return $className;
  }

  /**
   * @param object $program
   * @param array $modules
   * @return object
   */
  public static function create(object $program, array $modules = []): object
  {
    self::$modules = $modules;

    $classNameSpace = __NAMESPACE__ . '\\Program\\';

    $className = $classNameSpace . self::prepareClassName($program->name);

    if (!class_exists($className)) {
      $className = $classNameSpace . 'Program';
    }

    return (new $className($program, $modules));
  }
}