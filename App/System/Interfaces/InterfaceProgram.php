<?php

namespace App\System\Interfaces;

interface InterfaceProgram
{
  public function hasProgram();

  public function programExecute(string $command);

  public function install();
}