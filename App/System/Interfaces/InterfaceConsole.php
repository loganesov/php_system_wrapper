<?php

namespace App\System\Interfaces;

interface InterfaceConsole
{
  public static function execute(string $command);
}