<?php

namespace App\System;

use JetBrains\PhpStorm\ArrayShape;

class Console implements \App\System\Interfaces\InterfaceConsole
{
  /**
   * @param string $command
   * @return array
   */
  #[ArrayShape(['output' => "array", 'status' => "int"])]
  public static function execute(string $command): array
  {
    $output = [];
    $status = 0;

    exec($command, $output, $status);

    return [
      'output' => $output,
      'status' => $status,
    ];
  }
}