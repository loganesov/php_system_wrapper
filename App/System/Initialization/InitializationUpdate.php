<?php

namespace App\System\Initialization;

use App\System\Base\InitializationBase;

class InitializationUpdate extends InitializationBase
{
  /**
   * @return void
   */
  public static function execute(): void
  {
    global $app_version;
  }
}