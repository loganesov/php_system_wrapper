<?php

namespace App\System\Initialization;

use App\System\Base\InitializationBase;
use App\System\Configs;
use App\System\Console;
use App\System\Files;
use App\System\Loader;
use App\System\OS;
use App\System\Storage;
use JetBrains\PhpStorm\NoReturn;

class InitializationConfigs extends InitializationBase
{
  /**
   * @return void
   */
  #[NoReturn]
  public static function execute(): void
  {
    $GLOBALS['loader'] = $loader = Loader::getInstance();
    $loader->load([\App\ROOT . '/App/System/Configs.php']);

    $GLOBALS['config'] = $config = Configs::getInstance();
    $config->load(\App\ROOT . '/App/Configs/app.json', 'app');

    $config->load(\App\ROOT . '/Projects/config.json', 'projects');

    $GLOBALS['php_modules'] = $php_modules = $config->init_path($config->get('app')->php_modules, \App\ROOT);
    $GLOBALS['system_files'] = $system_files = $config->init_path($config->get('app')->system_files, \App\ROOT);

    $loader->load([$php_modules, $system_files]);

    $GLOBALS['console'] = Console::class;
    $GLOBALS['files'] = Files::class;

    $GLOBALS['projects_settings'] = $config->get('app')->projects_settings;

    $GLOBALS['app_version'] = $config->get('app')->app_version;

    $GLOBALS['OS'] = OS::getInstance((new Console()))->getConfig();

    $GLOBALS['Storage'] = Storage::getInstance((new Console()))->getConfig();
  
    $GLOBALS['uniq_id'] = \App\System\UniqID::getInstance(\App\ROOT . $config->get('app')->uniq_id_file_path);
    

//    $GLOBALS['path'] = $path = $config->init_path($config->get('config')->path, \App\ROOT);

    $config->set('programs', 'list', $config->get('app')->programs);
    $GLOBALS['programs']['list'] = $config->get('app')->programs->required;
    $GLOBALS['programs']['installed'] = false;
  }
}