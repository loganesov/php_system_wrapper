<?php

namespace App\System\Initialization;

use App\Modules\LoopProcessInfo;

class Initialization
{
  /**
   * @return void
   */
  public static function run(): void
  {
    $initialization_list = [
      'InitializationConfigs',
      'InitializationPrograms',
      'InitializationUpdate',
    ];

    $count = count($initialization_list);

    foreach ($initialization_list as $number => $initializationClassName) {
      call_user_func([__NAMESPACE__ . '\\' . $initializationClassName, 'execute']);

      LoopProcessInfo::render([
        'current' => ++$number,
        'all' => $count,
        'title' => '',
        'subtitle' => 'Initialization: ' . $initializationClassName,
      ]);
    }
    
//    echo '<pre>'; print_r($GLOBALS); echo '</pre>'; exit();
  }
}