<?php

namespace App\System\Initialization;

use App\System\ProgramList;
use App\System\Base\InitializationBase;

class InitializationPrograms extends InitializationBase
{
  /**
   * @return void
   */
  public static function execute(): void
  {
    global $programs;

    $program_list = new ProgramList($programs['list']);

    $program_list->initialization();

    $required_programs['installed'] = $program_list->getInstalled();
  }
}