<?php

namespace App\System;

class Loader
{
  private array $loaded_files = [];
  private array $state_list = ['in_standby', 'loading', 'loaded'];
  private string $state = 'in_standby';
  
  private static ?Loader $instance = null;
  
  public static function getInstance(): Loader
  {
    if (static::$instance === null) {
      static::$instance = new static();
    }
    
    return static::$instance;
  }
  
  private function __construct()
  {
  }
  
  private function __clone()
  {
  }
  
  public function getState(): string
  {
    return $this->state;
  }

  private function _loadFile($file) {
    if (file_exists($file)) {
      $this->loaded_files[] = $file;
      include_once($file);
    }
  }

  private function _prepareLoadFile($path) {
    if (is_dir($path)) {
      $files = array_map(function ($file) use($path) {
        return $path . $file;
      }, array_diff(scandir($path), array('..', '.')));

      $this->load($files);
    } else {
      $this->_loadFile($path);
    }
  }

  public function load($list = []): bool
  {
    if (empty($list)) {
      return false;
    }
    
    $is_loaded = true;

    if (is_array($list)) {
      foreach ($list as $path) {
        $this->_prepareLoadFile($path);
      }
    } else {
      $this->_prepareLoadFile($list);
    }

    return $is_loaded;
  }
  
  public function getLoadedFiles(): array
  {
    return $this->loaded_files;
  }
  
  
  // --- WORK CODE, NOT COMPLETED
  
  private function setStateLoaded()
  {
    $this->state = $this->getStateLoaded();
  }
  
  private function setStateLoading()
  {
    $this->state = $this->getStateLoading();
  }
  
  private function setStateInStandBy()
  {
    $this->state = $this->getStateInStandBy();
  }
  
  private function getStateLoaded()
  {
    return $this->state_list[2];
  }
  
  private function getStateLoading()
  {
    return $this->state_list[1];
  }
  
  private function getStateInStandBy()
  {
    return $this->state_list[0];
  }
  
  public function isLoaded()
  {
    return ($this->getState() === $this->getStateLoaded());
  }
}