<?php

namespace App\System\Base;

class InitializationBase
{
  private static array $dependencies = [];

  private static object|null $instance = null;

  public function __constructor()
  {
    return (self::$instance || self::instance());
  }

  public static function instance(): object
  {
    if (is_null(self::$instance)) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  public static function execute()
  {
  }
}