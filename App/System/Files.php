<?php

namespace App\System;

class Files
{
  private array $path;
  private array $files = [];
  private string $type;
  
  public function __construct(string $path, string $file_type)
  {
    $this->path = $path;
    $this->type = $file_type;
    
    if (empty($this->path) && empty($this->type)) {
      echo "Error in Path or Type";
      exit();
    }
    
    $this->files = $this->read_files();
    
    if (empty($this->files)) {
      echo "Files not found in " . $this->path['original'];
      exit();
    }
    
    $this->init();
  }
  
  public function read_files(string $path = null): array
  {
    return array_merge([], array_diff(scandir($path ?? $this->path['original']), array('.', '..')));
  }
  
  private function init(): bool
  {
    if ($this->clear($this->path['work'])) {
      return $this->copy($this->path['original'] . '/', $this->path['work'] . '/');
    }
    
    return false;
  }
  
  public function clear(string $directory, array $list = null): bool
  {
    $list = $list ?? $this->files;
    $is_completed = true;
    
    foreach ($list as $file) {
      $file_path = $directory . '/' . $file;
      if (file_exists($file_path) && !unlink($directory . '/' . $file)) {
        $is_completed = false;
      }
    }
    
    return $is_completed;
  }
  
  public function copy(string $from, string $to, array $list = null): bool
  {
    $list = $list ?? $this->files;
    $is_completed = true;
    
    foreach ($list as $file) {
      if (!copy($from . $file, $to . $file)) {
        $is_completed = false;
      }
    }
    
    return $is_completed;
  }
  
  public function path(): array
  {
    return $this->path;
  }
  
  public function files(): array
  {
    return $this->files;
  }
  
}