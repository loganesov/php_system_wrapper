<?php

namespace App\System;

use \App\System\Console;
use \App\Middleware\System\Execute;

class OS
{
  use Execute;

  private static array $config;

  private static object $console;

  private static object|null $instance = null;

  public function __constructor(Console $console)
  {
    return (self::$instance || self::getInstance($console));
  }

  public static function getInstance(Console $console): object
  {
    if (is_null(self::$instance)) {
      self::$instance = new self();

      self::$console = $console;
      self::initConfig();
    }

    return self::$instance;
  }

  private static function initConfig(): void
  {
    self::$config = [
      'name' => trim(str_replace('Description:', '', self::execute('lsb_release -d'))),
      'hostname' => self::execute('uname -n'),
    ];
  }

  public function getConfig(): array
  {
    return self::$config;
  }
}