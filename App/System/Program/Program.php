<?php

namespace App\System\Program;

use App\System\Console;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Program implements \App\System\Interfaces\InterfaceProgram
{
  protected string $name;

  protected string $version;

  protected string $programName;

  protected string $consoleProgramName;

  protected array $modules = [];

  /**
   * @param object $program
   * @param array $modules
   */
  public function __construct(object $program, array $modules)
  {
    $this->setModules($modules);

    $this->setName($program->name);
    $this->setVersion($program->version);
    $this->setProgramName();
    $this->setConsoleProgramName();
  }

  /**
   * @return string
   */
  public function getVersion(): string
  {
    return $this->version;
  }

  /**
   * @param string $version
   */
  protected function setVersion(string $version): void
  {
    $this->version = $version;
  }

  /**
   * @param array $modules
   */
  protected function setModules(array $modules): void
  {
    $this->modules = $modules ?? [];
  }

  /**
   * @param string $name
   */
  protected function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * @param string $name
   */
  protected function setProgramName(string $name = null): void
  {
    $this->programName = strtolower($name ?? $this->getName());
  }

  /**
   * @param string $name
   */
  public function setConsoleProgramName(string $name = null): void
  {
    $this->consoleProgramName = strtolower($name ?? $this->getName());
  }


  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getProgramName(): string
  {
    return $this->programName;
  }

  /**
   * @return string
   */
  public function getConsoleProgramName(): string
  {
    return $this->consoleProgramName;
  }

  /**
   * @param string $name
   * @return object
   */
  protected function getModules(string $name): object
  {
    return new $this->modules[$name];
  }


  /**
   * @return bool
   */
  public function hasProgram(): bool
  {
    $grep = ' | grep -o "' . $this->getProgramName() . '" | tail -n  1 ';
    $command = 'dpkg --get-selections ' . $grep;
    $consoleResponse = $this->consoleExecute($command);

    if ($this->issetProgram($consoleResponse['output'])) {
      return true;
    } else {
      $consoleResponse = $this->programExecute('-v' . $grep);

      return $this->issetProgram($consoleResponse['output']);
    }
  }

  /**
   * @param $output
   * @return bool
   */
  #[Pure]
  private function issetProgram($output): bool
  {
    return (
      !empty($output) &&
      in_array($this->getProgramName(), $output)
    );
  }


  /**
   * @param string $command
   * @return array
   */
  #[ArrayShape(['output' => "array", 'status' => "int"])]
  public function programExecute(string $command): array
  {
    return $this->getModules('console')::execute($this->getConsoleProgramName() . ' ' . $command);
  }

  protected function consoleExecute(string $command): array
  {
    return $this->getModules('console')::execute($command);
  }


  /**
   * @return bool
   */
  public function install(): bool
  {
    if ($this->hasProgram()) {
      return true;
    }

    $command = 'sudo apt-get update \ && echo "Need Install" \ && sudo apt install -y ' . $this->getProgramName();

    $this->consoleExecute($command);

    return $this->hasProgram();
  }
}
