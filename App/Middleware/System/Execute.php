<?php

namespace App\Middleware\System;

trait Execute
{
  private static function execute(string $command): string
  {
    if (empty($command)) {
      return '';
    }

    $output = self::$console::execute($command);

    if (!empty($output['output'])) {
      return trim(array_shift($output['output']));
    }

    return $output['status'];
  }
}