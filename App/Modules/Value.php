<?php

namespace App\Modules;

class Value
{
  /**
   * Returned value if not empty.
   * If empty string, then returned empty or set value in $defaultValue
   *
   * @param object|array $data
   * @param string $key
   * @param mixed $defaultValue
   * @return mixed
   */
  public static function check(object|array $data, string $key = '', mixed $defaultValue = ''): mixed
  {
    if (is_object($data)) {
      return (isset($data->$key) && !empty($data->$key)) ? $data->$key : $defaultValue;
    } else if (is_array($data)) {
      return (isset($data[$key]) && !empty($data[$key])) ? $data[$key] : $defaultValue;
    }
    return $defaultValue;
  }
}