<?php

namespace App\Modules;

class LoopProcessInfo
{
  /**
   * @param int $count
   * @param string $text
   * @return string
   */
  protected static function print_looping_text(int $count, string $text): string
  {
    $str = '';
    
    for ($i = 0; $i < $count; $i++) {
      $str .= $text;
    }
    
    return $str;
  }
  
  /**
   * @param int $current
   * @param int $all
   * @return string
   */
  protected static function loading_bar(int $current, int $all): string
  {
    $percent_all = '100%';
    $percent_left = ($current / $all) * 100;
    
    $current_looping_text = self::print_looping_text($current, '-');
    $left_looping_text = self::print_looping_text(($all - $current), ' ');
    
    return 'Proccess: ' . round($percent_left) . '% [' . $current_looping_text . $left_looping_text . '] ' . $percent_all;
  }
  
  /**
   * @param $data
   * @return void
   */
  public static function render($data): void
  {
    system('clear');
    
    echo "\n" . ($data['title'] ?? '');
    echo "\n" . ($data['subtitle'] ?? '') . "\n\n";
    echo $data['current'] . ' from ' . $data['all'] . "\n";
    echo self::loading_bar($data['current'], $data['all']) . "\n\n";
  }
}
