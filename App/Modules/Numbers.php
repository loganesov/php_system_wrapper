<?php

namespace App\Modules;

class Numbers
{
  public static function addZero($num): string
  {
    return str_pad($num, 4 , '0', STR_PAD_LEFT);
  }
}