<?php

namespace App\Modules;

/**
 * Класс для работы с csv-файлами
 */
class CSV
{
  private string|null $_csv_file = null;
  
  private string $delimeter = ',';
  
  /**
   * @param string $csv_file - путь до csv-файла
   */
  public function __construct($data)
  {
    if (file_exists($data['csv_file'])) {
      $this->_csv_file = $data['csv_file'];
      $this->delimeter = $data['delimeter'] ?? $this->delimeter;
    } else {
      throw new \Exception("Файл " . $data['csv_file'] . " не найден");
    }
  }
  
  /**
   * @param array $csv
   * @param string $modify (a - append | w - new )
   */
  public function setCSV(array $csv, $modify = 'a'): void
  {
    $handle = fopen($this->_csv_file, $modify . 'b');
    
    foreach ($csv as $value) {
      $this->putCSV($handle, $value);
    }
    
    fclose($handle);
  }
  
  private function putCSV(&$handle, $value): void
  {
    if (is_string($value)) {
      fputcsv($handle, explode($this->delimeter, $value), $this->delimeter);
    } else if (is_array($value)) {
      fputcsv($handle, $value);
    }
  }
  
  /**
   * Метод для чтения из csv-файла. Возвращает массив с данными из csv
   * @return array;
   */
  public function getCSV(): array
  {
    $handle = fopen($this->_csv_file, "r");
    
    $array_line_full = array();
    
    while (($line = fgetcsv($handle, 0, $this->delimeter)) !== FALSE) {
      $array_line_full[] = $line;
    }
    
    fclose($handle);
    
    return $array_line_full;
  }
}
