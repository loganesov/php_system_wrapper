<?php

namespace App\Modules;

class Text
{
  public static function convertDashToUpperCase(string $raw_text, string $separator): string
  {
    if (empty($raw_text)) {
      return '';
    }

    $word_list = explode($separator, $raw_text);

    $text = '';

    foreach ($word_list as $word) {
      $text .= ucfirst($word);
    }

    return $text;
  }
}