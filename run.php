<?php

namespace App;

if (PHP_SAPI !== 'cli') {
  exit("Runs only from Cli!\n");
}

if ((int)phpversion() < 8) {
  exit("Runs only from PHP8!\n");
}

const ROOT = __DIR__;

include ROOT . '/vendor/autoload.php';

\App\System\Initialization\Initialization::run();

global $config;


echo '<pre>'; var_dump($config->get('projects')); echo '</pre>';

exit();
